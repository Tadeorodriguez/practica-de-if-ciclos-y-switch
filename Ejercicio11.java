package ejerciciosIf;
import java.util.Scanner;
public class Ejercicio11 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		char caracter; 
        char a = 'a';
        char e = 'e';
        char i = 'i';
        char o = 'o';
        char u = 'u';
        	
        System.out.println("Introduce un letra: ");
        caracter = sc.next().charAt(0);
        
        if(caracter == a || caracter == e || caracter == i || caracter == o || caracter == u) {
        	System.out.println("La leta " + caracter + " es una vocal");
        }
        else {
        	System.out.println("La letra " + caracter + " no es una vocal");
        }
	}
}