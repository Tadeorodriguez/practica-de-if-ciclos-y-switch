package ejerciciosIf;
import java.util.Scanner;
public class Ejercicio13 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Ingrese el numero de un mes:  ");
		int mes = sc.nextInt();		
		while(mes < 1 || mes > 12) {
			System.out.println("Este numero no pertenece a un mes " );
			System.out.println("Ingrese otro numero: ");
			 mes = sc.nextInt();
		}
		switch(mes) {
		case 1:
			System.out.println("El mes " + mes + " es Enero y tiene " + 31 + " dias");
		break;
		
		case 2:
			System.out.println("El mes " + mes + " es Febrero y tiene " + 28 + " dias");
		break;
		
		case 3: 
			System.out.println("El mes " + mes + " es Marzo y tiene " + 31 + " dias");
		break;
		
		case 4:
			System.out.println("El mes " + mes + " es Abril y tiene " + 30 + " dias");
			break;
			
		case 5:
			System.out.println("El mes " + mes + " es Mayo y tiene " + 31 + " dias");
			break;
			
		case 6:
			System.out.println("El mes " + mes + " es Junio y tiene " + 30 + " dias");
			break;
			
		case 7:
			System.out.println("El mes " + mes + " es Julio y tiene " + 31 + " dias");
			break;
			
		case 8:
			System.out.println("El mes " + mes + " es Agosto y tiene " + 31 + " dias");
			break;
			
		case 9:
			System.out.println("El mes " + mes + " es Septiembre y tiene " + 30 + " dias");
			break;
		case 10:
			System.out.println("El mes " + mes + " es Octubre y tiene " + 31 + " dias");
			break;
			
		case 11:
			System.out.println("El mes " + mes + " es Noviembre y tiene " + 30 + " dias");
			break;
			
		case 12:
			System.out.println("El mes " + mes + " es Diciembre y tiene " + 31 + " dias");
			break;
		}
	}
}